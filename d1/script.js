/*
Steps in designing a database
1. what are Objects and attributes.

2. what are the relationship among these objects?
	a. One to many- 1 playlist with many songs
	b. One to One- one user with single playlist
	c. Many to many - multiple playlist containing similar multiple songs

3. Create entity relationship diagram
-shows relationship of entity sets stored in database. entity is an object, a component of a data. An entity set is a collection of similar entities. These entities can have attributes that define its properties
	
	Common entity relationship diagram symbols
		a.Rectangle- Object- artist
		b.Oval- attribute-  name
		c.Diamond- actions- create

4. Convert our diagram into tables
	a.Create a table for each entity(rectangle) ith the attributes(ovals) as the columns
	b.Include a primary key column id in the created table, to uniquely identify all table records
	c. for one to many, add a foreign key. 
			Album
	id |name| year| artist_id

artist                         album
id |name					id |  name | 			| year | artist_id
1 	LP						1	Meteora					   |	1
2	NSNC					2	Minutes to midnight		   |	1
							3	No strings attached		   |    2

What is a MongoDB Atlas?

- MongoDB atlas is a mongoDB database located in the cloud. 

*/							